# OS Bootstrap

Script for bootstrapping a fresh OS install with Bitwarden vault and Gitlab SSH keys.

## Dependencies

- Python 3
- wget
- curl
- unzip
- ssh-keygen
