import sys
import os
import json
import shutil

from subprocess import run
from pathlib import Path
from tempfile import TemporaryDirectory
from socket import gethostname


def ensure_dependencies():
    dependencies = ('curl', 'wget', 'unzip', 'ssh-keygen')
    missing = tuple(shutil.which(d) is None for d in dependencies)

    if not any(missing):
        return

    required = '\n'.join(d for d, m in zip(dependencies, missing) if m)
    sys.exit('Missing required applications! '
             f'Please install the following applications: \n{required}')


def download_bitwarden(dl_dir=None):
    preinstalled = shutil.which('bw')

    if preinstalled is not None:
        return Path(preinstalled)

    cwd_executable = Path('bw')

    if cwd_executable.is_file:
        cwd_executable.chmod(0o755)
        return cwd_executable

    url = 'https://vault.bitwarden.com/download/?app=cli&platform=linux'

    with TemporaryDirectory() as tmpdir:
        p = run(('wget', '--content-disposition', url), cwd=tmpdir)

        if p.returncode != 0:
            sys.exit(f'ERROR ({p.returncode}) downloading {url}!')

        zipfile, = Path(tmpdir).glob('*.zip')

        p = run(('unzip', zipfile), cwd=dl_dir)

        if p.returncode != 0:
            sys.exit(f'ERROR ({p.returncode}) unzipping {zipfile}!')

    bw_executable = Path('bw')

    if not bw_executable.exists():
        sys.exit(f'ERROR! Cannot find: {bw_executable.name}')

    bw_executable.chmod(0o755)

    return bw_executable


def set_bw_session_token(bw_dir=None):
    email = input('Enter gitlab user email: ').strip()
    # print prompt since stdout is captured
    print('Master password: [hidden]')
    p = run(('bw', 'login', email, '--raw'),
            stdin=True, capture_output=True, cwd=bw_dir)

    if p.returncode != 0:
        sys.exit(f'ERROR ({p.returncode}) unlocking bitwarden!')

    os.environ['BW_SESSION'] = p.stdout.decode().strip()


def generate_ssh_keypair(keyfile=None):
    keypath = Path('~/.ssh/id_rsa').expanduser() if keyfile is None else keyfile

    if keypath.exists():
        return keypath

    cmd = ['ssh-keygen']

    if keyfile is not None:
        cmd.extend(['-f', str(keyfile)])

    p = run(cmd, stdin=True)

    if p.returncode != 0:
        sys.exit(f'ERROR generating ssh keypair!')

    return keypath


def get_gitlab_api_token():
    bw_item = input('Enter bitwarden item name: ')
    cmd = ('bw', 'get', 'item', bw_item)
    p = run(cmd, capture_output=True, text=True)

    if p.returncode != 0:
        sys.exit(f'ERROR fetching gitlab api token from bitwarden vault!')

    result = json.loads(p.stdout.rstrip())
    notes = result.get('notes', '')

    if notes != '':
        return notes

    return result['login']['password']


def push_ssh_pubkey(gitlab_api_token, ssh_keyfile, ssh_keyname):
    if ssh_keyfile.suffix != '.pub':
        pubkey = ssh_keyfile.with_suffix('.pub')
    else:
        pubkey = ssh_keyfile

    with open(pubkey) as f:
        data = {
            'title': ssh_keyname,
            'key': f.read(),
        }

    cmd = ('curl',
           '--request', 'POST',
           '--header', 'Content-Type: application/json',
           '--header', f'Private-Token: {gitlab_api_token}',
           '--data', json.dumps(data),
           'https://gitlab.com/api/v4/user/keys')

    p = run(cmd, text=True)

    if p.returncode != 0:
        sys.exit(f'ERROR generating ssh keypair!')

    print(json.dumps(p.stdout, indent=2))


def main():
    msg = ('This script will bootstrap bitwarden and gitlab ssh key.\n'
           'Continue? [y/N] ')
    proceed = input(msg)

    if proceed.lower() not in ('y', 'yes', 'ok', 'true'):
        sys.exit()

    ensure_dependencies()
    bw_executable = download_bitwarden()
    set_bw_session_token(bw_executable.parent)
    keyfile = generate_ssh_keypair()
    gitlab_ssh_keyname = input(f'Enter gitlab ssh key name: [{gethostname()}]')

    if gitlab_ssh_keyname == '':
        gitlab_ssh_keyname = gethostname()

    push_ssh_pubkey(get_gitlab_api_token(), keyfile, gitlab_ssh_keyname)


if __name__ == '__main__':
    main()
